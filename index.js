const express = require('express');
const app = express();

app.get('/', (req, res) => {
    res.send("Hello from Node Application");
});

const PORT = process.env.PORT || 3003;

const server = app.listen(PORT, () => {
    console.log("Listening on port", server.address().port);
});